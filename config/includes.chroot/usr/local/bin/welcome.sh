#!/bin/bash
# welcome script
# remove kernel if needed
# fix bookmarks
##################################

# détection de la langue
TRAD=`echo "${LANG:0:2}"`

if [ "${TRAD}" == "fr" ]; then
    #langue française
    cd /usr/local/bin/welcome
    #session live
    if [ -d /home/humain ]; then
        ../keyboard_selector
        ./welcome.py
    #session installée
    else
        echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Images"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Musique"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/T%C3%A9l%C3%A9chargements" >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Vid%C3%A9os" >> /home/$USER/.gtk-bookmarks
        ./welcome.py
        rm /home/$USER/.config/autostart/welcome.desktop
        rm /home/$USER/.magnifier.ini.en
        rm /home/$USER/.config/user-dirs.dirs.en
        cat /proc/cpuinfo | grep flags > /tmp/paeornot
        if grep -q pae /tmp/paeornot; then ../486-kernel-cleaner-launcher; fi
        rm /tmp/paeornot
        python ./handyupdate.py
    fi
else
    #english language
    cd /usr/local/bin/welcome-en
    #live session
    if [ -d /home/human ]; then
        mv /home/$USER/Images /home/$USER/Pictures
        mv /home/$USER/Musique /home/$USER/Music
        mv /home/$USER/Téléchargements /home/$USER/Downloads
        mv /home/$USER/Vidéos /home/$USER/Videos
        mv /home/$USER/Modèles /home/$USER/Templates
        echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Pictures"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Music"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Downloads" >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Videos" >> /home/$USER/.gtk-bookmarks
        rm /home/$USER/.config/user-dirs.dirs
        mv /home/$USER/.config/user-dirs.dirs.en /home/$USER/.config/user-dirs.dirs
        rm /home/$USER/.config/user-dirs.locale
        rm /home/$USER/.magnifier.ini
        mv /home/$USER/.magnifier.ini.en /home/$USER/.magnifier.ini
        ../keyboard_selector
        ./welcome.py
    else
    #installed session
        mv /home/$USER/Images /home/$USER/Pictures
        mv /home/$USER/Musique /home/$USER/Music
        mv /home/$USER/Téléchargements /home/$USER/Downloads
        mv /home/$USER/Vidéos /home/$USER/Videos
        mv /home/$USER/Modèles /home/$USER/Templates
        echo "file:///home/$USER/Documents" > /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Pictures"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Music"  >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Downloads" >> /home/$USER/.gtk-bookmarks
        echo "file:///home/$USER/Videos" >> /home/$USER/.gtk-bookmarks
        rm /home/$USER/.config/user-dirs.dirs
        mv /home/$USER/.config/user-dirs.dirs.en /home/$USER/.config/user-dirs.dirs
        rm /home/$USER/.config/user-dirs.locale
        rm /home/$USER/.magnifier.ini
        mv /home/$USER/.magnifier.ini.en /home/$USER/.magnifier.ini
        ./welcome.py
        rm /home/$USER/.config/autostart/welcome.desktop
        cat /proc/cpuinfo | grep flags > /tmp/paeornot
        if grep -q pae /tmp/paeornot; then ../486-kernel-cleaner-launcher; fi
        rm /tmp/paeornot
        python ./handyupdate.py
    fi
fi
exit 0
