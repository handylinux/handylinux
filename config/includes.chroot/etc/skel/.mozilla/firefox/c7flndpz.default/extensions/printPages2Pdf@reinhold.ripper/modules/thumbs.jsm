Components.utils.import("resource://printPages2PdfMod/printPages2PdfGlobal.jsm"); 

var EXPORTED_SYMBOLS = [ "_thumbs", ];


/** 
 * printPages2Pdf namespace. 
 */  
 
if (typeof printPages2Pdf == "undefined") {  
  var printPages2Pdf = {};  
};

/*
_thumbs = function(doc,pdfList,callback,objArgs) {
	this._doc = doc;
	this._pdfList = pdfList;
	this._cb = callback;
	this._objArgs = objArgs;
	this.createThumbs();
}	
	*/
_thumbs = function(frame) {
	this.iframe = frame;
}	
	
_thumbs.prototype = {
	iframe_id : "ifrthumb",
	iframe : null,
	procList : new Array(),
	frameLoaded : false,
	processing : false,
	
	create : function(frame,pdfFile,callback,objArgs){ //should be called static (.prototype.....
		if(!frame.objProc)
			frame.objProc=new _thumbs(frame);
		
		frame.objProc.process(pdfFile,callback,objArgs);
	},
	
	process : function(pdfFile,callback,objArgs){
		var proc={pdfFile:pdfFile,callback:callback,objArgs:objArgs};
		this.procList.push(proc);

		if(!this.frameLoaded){
			var src=this.iframe.getAttribute("src");
			if ((!src) || src != "chrome://printPages2Pdf/content/thumbs.html") {
				this.iframe.setAttribute("src", "chrome://printPages2Pdf/content/thumbs.html");
			}
		}
		
		this.execCreate();			
	},
	
	frameReady : function(){
		this.frameLoaded=true;
		this.execCreate();
	},
	
	execCreate : function(){		
		if(this.procList.length <= 0 || this.processing === true || this.frameLoaded === false ) return;
		this.processing=true;

		var proc=this.procList.pop();
		var IO=Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
		var pdfUrl=IO.newFileURI(proc.pdfFile).spec;
			
		var imgTrg=RRprintPages2Pdf.getThumbFile(proc.pdfFile,"png");
			
		var scale=RRprintPages2Pdf.prefs.getCharPref("archive.thumbsquality");
		scale=Math.round(scale) / 100;
		var obj={pdfSrc:pdfUrl, trgFile:imgTrg,controller:this, callback:proc.callback,objArgs:proc.objArgs,scale:scale};	
		this.iframe.contentWindow.loadPDF(obj);
					
	},
	
	createFinished : function(myProc){
		if(myProc.callback != null)
			myProc.callback.displayThumb(myProc.trgFile,myProc.objArgs);
		
		this.processing=false;
		this.execCreate();
	},
	
}	
	

