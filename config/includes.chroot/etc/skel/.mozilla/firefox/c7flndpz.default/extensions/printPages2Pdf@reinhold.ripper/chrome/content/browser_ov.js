Components.utils.import("resource://printPages2PdfMod/printPages2PdfGlobal.jsm"); 

/** 
 * printPages2Pdf namespace. 
 */  
 
if (typeof printPages2Pdf == "undefined") {  
  var printPages2Pdf = {};  
};

Components.utils.import("resource://printPages2PdfMod/srcObjectLight.jsm",printPages2Pdf);
	
printPages2Pdf.UI = {
	_dialogWin:null,
	
	onPopupShowing:function(parent){
		var frMen=document.getElementById("printPages2Pdf_printFrame");
		if(gContextMenu.inFrame){
			frMen.hidden=false;
			frMen.addEventListener("command",function(){
				printPages2Pdf.UI.printFrame(document.popupNode.ownerDocument.defaultView);
			},
			false
			);
		}
		else
			frMen.hidden=true;

		var lnkMen=document.getElementById("printPages2Pdf_printDocLink");
		if(gContextMenu.onLink && !gContextMenu.onMailtoLink){
			lnkMen.hidden=false;
			lnkMen.addEventListener("command",function(){
				printPages2Pdf.UI.printLink(document.popupNode);
			},
			false
			);
		}
		else
			lnkMen.hidden=true;

		
	},
	
	onToolbarButton:function(evt){
		switch (RRprintPages2Pdf.prefs.getCharPref("ui.tbbfunction")) {
			case "activetab":
				this.printActiveTab({
					g_textOnly: false,
				});
				break;
			case "alltabs":
				this.printAllTabs({
					g_textOnly: false,
				});
				break;
			default:
				break;
		}
	},


	printSelectedTabs : function(pars){

		if(! MultipleTabService) return;
		
		var tabs=MultipleTabService.getSelectedTabs();
		
		if(tabs.length === 0 ) return;
		
		var convWins=new Array();
		
		for (var i = 0; i < tabs.length; i++) {
			var srcObjectLight = new printPages2Pdf._srcObjectLight(gBrowser.getBrowserForTab(tabs[i]).contentWindow, pars.g_textOnly);
			var title = gBrowser.getBrowserForTab(tabs[i]).contentTitle;
			if(title)
				srcObjectLight.Title = title;
			
			srcObjectLight.sourceType="browsertab";
			convWins.push(srcObjectLight);
		}
			
		
		RRprintPages2Pdf.startConversionDlg(convWins,pars,window);
				
	},

	
	printAllTabs : function(pars){

		if(gBrowser.browsers.length == 0) return;

		
		var convWins=new Array();
		
		for (var i = 0; i < gBrowser.browsers.length; i++) {
			//convWins.push(gBrowser.browsers[i].contentWindow);
			
			var srcObjectLight = new printPages2Pdf._srcObjectLight(gBrowser.browsers[i].contentWindow, pars.g_textOnly);
			var title = gBrowser.browsers[i].contentTitle;
			if(title)
				srcObjectLight.Title = title;
			
			srcObjectLight.sourceType="browsertab";
			convWins.push(srcObjectLight);
		}
			
		
		RRprintPages2Pdf.startConversionDlg(convWins,pars,window);
				
	},
	
	handleKeyboardShortcut:function(event){
	
		var menu=document.getElementById('printPages2Pdf_extraMenuPopup');

		var x=0; var y=0;

		if(gBrowser.selectedBrowser){
			x=gBrowser.selectedBrowser.contentWindow.outerWidth / 2;
			y=gBrowser.selectedBrowser.contentWindow.outerHeight / 2;
		}
		else
		{
			x=window.outerWidth / 2;
			y=window.outerHeight / 2;
		}
		menu.openPopup(null,null,x,y,true,true);
	},

	openProcessDialog:function(event){
		RRprintPages2Pdf.startConversionDlg([],{},window);		
	},
	
	openPreferences: function(event){
		var features = "chrome,titlebar,toolbar,centerscreen,modal";
		window.openDialog("chrome://printPages2Pdf/content/prefs.xul", "Preferences", features);
		
	},
	
	printLink:function(el){
		var srcObjectLight = new printPages2Pdf._srcObjectLight(el.toString(), false);
		

		srcObjectLight.sourceType="unknown";

		RRprintPages2Pdf.startConversionDlg([srcObjectLight],{g_textOnly:false,},window);
	},

	printFrame:function(frmWindow){
		var srcObjectLight = new printPages2Pdf._srcObjectLight(frmWindow, false);
		

		var title = frmWindow.document.title;
		if(title)
			srcObjectLight.Title = title;

	
		srcObjectLight.sourceType="frame";

		RRprintPages2Pdf.startConversionDlg([srcObjectLight],{g_textOnly:false,},window);
		this._frame=null;
	},
	
	printThisTab:function(pars,tab){
		if (!tab) return;
		
		var srcObjectLight = new printPages2Pdf._srcObjectLight(gBrowser.getBrowserForTab(tab).contentWindow, pars.g_textOnly);
		var title = gBrowser.getBrowserForTab(tab).contentTitle;
		if(title)
			srcObjectLight.Title = title;
		
		srcObjectLight.sourceType="browsertab";

		
		RRprintPages2Pdf.startConversionDlg([srcObjectLight],pars,window);
	},


	toggleSidebar : function(pars){
		
		var sidebarWindow = document.getElementById("sidebar").contentWindow;
		//sidebarWindow.location.href = "chrome://printPages2Pdf/content/sbArchive.xul";
		toggleSidebar(pars);		
		
	},
	
	printActiveTab : function(pars){

		if(gBrowser.browsers.length == 0 || gBrowser.selectedTab == null) return;

		
		
		var srcObjectLight = new printPages2Pdf._srcObjectLight(gBrowser.selectedBrowser.contentWindow, pars.g_textOnly);
		var title = gBrowser.selectedBrowser.contentTitle;
		if(title)
			srcObjectLight.Title = title;
		
		srcObjectLight.sourceType="browsertab";

		
		RRprintPages2Pdf.startConversionDlg([srcObjectLight],pars,window);
				
	},

	onTest : function(event){
		var tabs=MultipleTabService.getSelectedTabs();
		
		alert(tabs);
		
	}	
}

window.addEventListener("load",function(){
	if (RRprintPages2Pdf.prefs.getBoolPref("ui.showpagemenus") === false)
	{
		var cmd=document.getElementById("printPages2Pdf_showpagemenu");
		if(cmd) cmd.setAttribute("hidden",true);
	}

	if (RRprintPages2Pdf.prefs.getBoolPref("ui.showextramenus") === false)
	{
		var cmd=document.getElementById("printPages2Pdf_showextramenu");
		if(cmd) cmd.setAttribute("hidden",true);
	}

	if (RRprintPages2Pdf.prefs.getBoolPref("ui.showtabmenu") === false)
	{
		var cmd=document.getElementById("printPages2Pdf-men_printThisTab");
		if(cmd) cmd.setAttribute("hidden",true);
	}
	

	var keyset=document.getElementById("mainKeyset");
	if (keyset) {
		RRprintPages2Pdf.updateShortcut(keyset,"menmain","printPages2Pdf_mainMenuKey",{
			oncommand:"printPages2Pdf.UI.handleKeyboardShortcut(event);",
			});

		
		RRprintPages2Pdf.updateShortcut(keyset,"sidebar","printPages2Pdf_sidebarKey",{
			observes:"printPages2Pdf-bc_sidebar",
			});

		RRprintPages2Pdf.updateShortcut(keyset,"prpage","printPages2Pdf_prpageKey",{
			oncommand:"printPages2Pdf.UI.printActiveTab({g_textOnly:false});",
			});

		RRprintPages2Pdf.updateShortcut(keyset,"prtabs","printPages2Pdf_prtabsKey",{
			oncommand:"printPages2Pdf.UI.printAllTabs({g_textOnly:false});",
			});
	}
	
},
false);	
