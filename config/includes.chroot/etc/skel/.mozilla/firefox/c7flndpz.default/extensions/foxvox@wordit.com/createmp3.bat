@ECHO OFF
REM  get mp3 file path and extension file paths, passed as command line args
set extpath=%1
set mp3path=%2
REM "%extpath%\ConsoleTool.exe" /HIDE
echo "extpath:" %extpath%
echo "encoder path:" %mp3path%
set encoderpath=%extpath%;\lameoggenc2.exe

REM  remove marker indicating to extension that mp3 file creation has completed
del %extpath%\mp3done.txt
del %extpath%\nolame.txt

REM  encode the existing wave as mp3 file
lame.exe %extpath%\speak.wav %mp3path%

set error=%ERRORLEVEL%
echo "lame error: %error%"

    IF %ERRORLEVEL% NEQ 0 (
    echo "Could not find lame"
    REM create marker file
    echo "mp3 lame not found" > %extpath%\nolame.txt
    ) ELSE (
    REM completed marker file
    echo "OK"
    echo "mp3 done" > %extpath%\mp3done.txt
    )

